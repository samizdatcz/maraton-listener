require! fs

ratio = 21097.5 / 0.23897204822768192

getDistance = (c1, c2) ->
  x = (c1.0 - c2.0) * Math.cos c2.1
  y = (c1.1 - c2.1)
  dst = ratio * Math.sqrt (x^2 + y^2)

getTrack = ->
  track = JSON.parse fs.readFileSync "#__dirname/../data/pulka.geojson"
  coordinates = track.features.0.geometry.coordinates.0
  totalDistance = 0

  coordinates.0.km = 0
  for i in [1 til coordinates.length]
    lastCoord = coordinates[i - 1]
    coord = coordinates[i]
    totalDistance += getDistance lastCoord, coord
    coord.km = totalDistance / 1000
  coordinates

track = getTrack!


module.exports.snapToTrack = snapToTrack = ([x, y]:coordinates, minKm, maxKm = minKm + 3) ->
  trackToUse = track
  if minKm?
    startIndex = null
    endIndex = void
    for point, index in track
      if startIndex == null and point.km >= minKm
        startIndex = index
      if point.km >= maxKm
        endIndex = index
        break
    trackToUse .= slice startIndex, endIndex

  currentDst = Infinity
  minDstPoint = null
  for point, index in trackToUse
    dst = getDistance point, coordinates
    continue if dst > 1000
    if dst <= currentDst
      minDstPoint = point
      currentDst = dst
  minDstPoint


# console.log snapToTrack [14.41559 50.08871], 19

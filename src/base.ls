require! http
require! "./config"
require! fs
require! querystring

history = fs.readFileSync "#__dirname/../data/out.txt" .toString!
outStream = fs.createWriteStream "#__dirname/../data/out.txt", flags: "a"

server = http.createServer (req, res) ->
  data = ""
  req.on \data -> data += it
  req.on \end ->
    ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
    if !data && -1 != req.url.indexOf '?'
      data = req.url.split '?' .1
    if data
      processData data
      saveData data
      updateOutput!
    process.stdout.write ">#{ip}\n#data\n"
    res.writeHead 200
    res.end!
server.listen config.port

lastLat = null
lastLon = null
lastLatLonTime = null

lastBeat = null
lastBeatTime = null

lastMox = null
lastMoxTime = null
processData = (data) ->
  lines = data
    .split "\n"
    .filter (.length)
  for line in lines
    data = querystring.parse line
    if data.lat and data.lon
      lastLat := parseFloat data.lat
      lastLon := parseFloat data.lon
      lastLatLonTime := data.time
    if data.db
      lastBeat := parseFloat that
      lastBeatTime := data.time
    if data.mox
      lastMox := parseFloat that
      lastMoxTime := data.time

saveData = (data) ->
  lines = data
    .split "\n"
    .filter (.length)
  outStream.write "#{lines.join "\n"}\n"

updateOutput = ->
  out =
    lat: lastLat
    lon: lastLon
    coordTime: lastLatLonTime
    beat: lastBeat
    beatTime: lastBeatTime
    mox: lastMox
    moxTime: lastMoxTime
  fs.writeFile "#__dirname/../data/output/lastCoords.json", JSON.stringify out

processData history
updateOutput!
